<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;



class AdminOrderController extends Controller
{


    /**
     * Finds and displays a commande entity.
     *
     * @Route("admin/order/{id}", name="commande_show")
     * @Method("GET")
     */
    public function showAction(Commande $commande)
    {
        $deleteForm= $this->get('app.back_order')->show($commande);

        return $this->render('back/commande/show.html.twig', array(
            'commande' => $commande,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing commande entity.
     *
     * @Route("admin/order/{id}/edit", name="commande_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Commande $commande)
    {

        $data = $this->get('app.back_order')->edit($request, $commande);

        if ($request->isMethod('POST') && $data === null) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('commande_edit', array('id' => $commande->getId()));
        }

        return $this->render('back/commande/edit.html.twig', array(
            'commande' => $commande,
            'delete_form' => $data[0]->createView(),
            'edit_form' => $data[1]->createView(),
        ));
    }

    /**
     * Deletes a commande entity.
     *
     * @Route("/admin/order/{id}/delete", name="commande_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Commande $commande)
    {

        $form = $this->get('app.back_order')->delete($request, $commande);

        return $this->redirectToRoute('adminArticles_index', array(
            'form' => $form,
        ));
    }

}
