<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prenom', TextType::class, array(
                'label' => 'Prénom',
                'constraints' => array(
                    new Length(array("min" => 3,
                        "max" => 50,
                        "minMessage" => "Il faut 3 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('nom', TextType::class, array(
                'label' => 'Nom',
                'constraints' => array(
                    new Length(array("min" => 2,
                        "max" => 50,
                        "minMessage" => "Il faut 2 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('email', EmailType::class, array(
                'constraints' => array(
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('objet', TextType::class, array(
                'constraints' => array(
                    new Length(array("min" => 5,
                        "max" => 50,
                        "minMessage" => "Il faut 5 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('message', TextareaType::class, array(
                'constraints' => array(
                    new Length(array("min" => 20,
                        "max" => 500,
                        "minMessage" => "Il faut 20 caractères minimum",
                        "maxMessage" => "Il faut 500 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('submit', SubmitType::class, array(
                'label' => 'Envoyer'
            ));
    }
}
