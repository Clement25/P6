<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


class ShopAdminType extends ShopType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Titre',
                'constraints' => array(
                    new Length(array("min" => 5,
                        "max" => 50,
                        "minMessage" => "Il faut 5 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('content', TextareaType::class, array(
                'label' => 'Contenu',
                'constraints' => array(
                    new Length(array("min" => 10,
                        "max" => 500,
                        "minMessage" => "Il faut 10 caractères minimum",
                        "maxMessage" => "Il faut 500 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
             ->add('envoyer', SubmitType::class);
    }


    public function getParent()
    {
        return ShopType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_shopAdmin';
    }


}
