<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setFirstName('Clement');
        $userAdmin->setName('MICHELIN');
        $userAdmin->setBirthDate(new \DateTime('05/10/1990'));
        $userAdmin->setValidMail(true);
        $userAdmin->setUsername('admin');
        $userAdmin->setCodeValidation('ezaiueeazkpe55OJE7okjzia');
        $userAdmin->setAccount('Administrateur');
        $userAdmin->setRoles(array('ROLE_ADMIN'));

        $this->cryptPassword($userAdmin, 'test');

        $manager->persist($userAdmin);
        $manager->flush();

        $userMembreAssociation = new User();
        $userMembreAssociation->setFirstName('Jean');
        $userMembreAssociation->setName('DUBOIS');
        $userMembreAssociation->setBirthDate(new \DateTime('05/08/1950'));
        $userMembreAssociation->setValidMail(true);
        $userMembreAssociation->setUsername('membre');
        $userMembreAssociation->setCodeValidation('razporjaojpjaz2opkraoj');
        $userMembreAssociation->setAccount('MEMBRE DE L\'ASSOCIATION');
        $userMembreAssociation->setRoles(array('ROLE_MEMBRE_ASSOCIATION'));

        $this->cryptPassword($userMembreAssociation, 'test');

        $manager->persist($userMembreAssociation);
        $manager->flush();

    }


    public function cryptPassword(User $user, $plainPassword)
    {

        $encoder = $this->container->get('security.password_encoder');

        $encoded = $encoder->encodePassword($user, $plainPassword);

        $user->setPassword($encoded);
    }


    public function getOrder()
    {
        return 5;
    }
}