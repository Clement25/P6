<?php

namespace AppBundle\Services;


use AppBundle\Entity\Shop;
use AppBundle\Form\ShopAdminType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;


class BackShop
{

    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var RouterInterface
     */
    private $router;


    public function __construct(
        EntityManager $doctrine,
        Session $session,
        FormFactory $form,
        RouterInterface $router
    )
    {
        $this->doctrine = $doctrine;
        $this->session = $session;
        $this->form = $form;
        $this->router = $router;
    }

    public function index()
    {
        $em = $this->doctrine;

        $shops = $em->getRepository('AppBundle:Shop')->findAll();
        $commandes = $em->getRepository('AppBundle:Commande')->findAll();

        return [$shops, $commandes];
    }


    public function add(Request $request)
    {

        $shop = new Shop();
        $form = $this->form->create(ShopAdminType::class, $shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shop->setDessinValid(true);
            $em = $this->doctrine;
            $em->persist($shop);
            $em->flush();
            $this->session->getFlashBag()->add('info', 'L\'article a bien été ajouter.');
        }

        return [$shop, $form];

    }


    public function show(Shop $shop)
    {
        $deleteForm = $this->createDeleteForm($shop);

        return $deleteForm;
    }


    public function edit(Request $request, Shop $shop)
    {
        $deleteForm = $this->createDeleteForm($shop);
        $editForm = $this->form->create(ShopAdminType::class, $shop);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->doctrine;
            $em->flush();
            $this->session->getFlashBag()->add('info', 'L\'article a bien été modifier.');
        }


        return [$deleteForm, $editForm, $shop];
    }


    public function delete(Request $request, Shop $shop)
    {
        $form = $this->createDeleteForm($shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->doctrine;
            $em->remove($shop);
            $em->flush();
            $this->session->getFlashBag()->add('info', 'L\'article a bien été supprimer.');
        }

        return $form;

    }

    private function createDeleteForm(Shop $shop)
    {
        return $this->form->createBuilder()
            ->setAction($this->router->generate('adminArticles_delete', array('id' => $shop->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


    public function addValidation(Request $request)
    {
        $idArticle = $request->attributes->get('id');

        $em = $this->doctrine;
        $article = $em->getRepository('AppBundle:Shop')->findOneBy(array('id' => $idArticle));

        if ($article) {
            $article->setDessinValid(true);

            $em->flush();
        }

        return $article;
    }

    public function removeValidation(Request $request)
    {
        $idArticle = $request->attributes->get('id');

        $em = $this->doctrine;
        $article = $em->getRepository('AppBundle:Shop')->findOneBy(array('id' => $idArticle));

        if ($article) {
            $article->setDessinValid(false);

            $em->flush();
        }

        return $article;

    }
}

