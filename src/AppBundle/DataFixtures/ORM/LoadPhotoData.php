<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Image;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadPhotoData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{


    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $image1 = new Image();
        $image1->setExtension('jpg');
        $image1->setAlt('image 1');

        $manager->persist($image1);


        $image2 = new Image();
        $image2->setExtension('jpg');
        $image2->setAlt('image 2');

        $manager->persist($image2);


        $image3 = new Image();
        $image3->setExtension('jpg');
        $image3->setAlt('image 3');

        $manager->persist($image3);


        $image4 = new Image();
        $image4->setExtension('jpg');
        $image4->setAlt('image 4');

        $manager->persist($image4);


        $image5 = new Image();
        $image5->setExtension('jpg');
        $image5->setAlt('image 5');

        $manager->persist($image5);


        $image6 = new Image();
        $image6->setExtension('jpg');
        $image6->setAlt('image 6');

        $manager->persist($image6);


        $manager->flush();

        $this->addReference('image1', $image1);
        $this->addReference('image2', $image2);
        $this->addReference('image3', $image3);
        $this->addReference('image4', $image4);
        $this->addReference('image5', $image5);
        $this->addReference('image6', $image6);
    }


    public function getOrder()
    {
        return 1;
    }
}