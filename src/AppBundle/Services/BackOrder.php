<?php

namespace AppBundle\Services;

use AppBundle\Entity\Commande;
use AppBundle\Form\CommandeType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;


class BackOrder
{

    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var RouterInterface
     */
    private $router;


    public function __construct(
        EntityManager $doctrine,
        Session $session,
        FormFactory $form,
        RouterInterface $router
    )
    {
        $this->doctrine = $doctrine;
        $this->session = $session;
        $this->form = $form;
        $this->router = $router;
    }

    public function show(Commande $commande)
    {
        $deleteForm = $this->createDeleteForm($commande);

        return $deleteForm;
    }


    public function edit(Request $request, Commande $commande)
    {

        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->form->create(CommandeType::class, $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->doctrine;
            $em->flush();
            $this->session->getFlashBag()->add('info', 'La commande a bien été modifier.');
        }


        return [$deleteForm, $editForm, $commande];
    }

    public function delete(Request $request, Commande $commande)
    {
        $form = $this->createDeleteForm($commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->doctrine;
            $em->remove($commande);
            $em->flush();
            $this->session->getFlashBag()->add('info', 'La commande a bien été supprimer.');
        }

        return $form;
    }


    private function createDeleteForm(Commande $commande)
    {
        return $this->form->createBuilder()
            ->setAction($this->router->generate('commande_delete', array('id' => $commande->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


}

