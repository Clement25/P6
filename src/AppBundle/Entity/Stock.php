<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stock
 *
 * @ORM\Table(name="stock")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StockRepository")
 */
class Stock
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var boolean
     *
     * @ORM\Column(name="available", type="boolean")
     */
    private $available;



    /**
     * @var integer
     *
     * @ORM\Column(name="Price", type="integer", nullable=false)
     */
    private $price;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Shop", inversedBy="stock", cascade={"persist"})
     */
    private $stock;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set stock
     *
     * @param \AppBundle\Entity\Shop $stock
     *
     * @return Stock
     */
    public function setStock(\AppBundle\Entity\Shop $stock = null)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \AppBundle\Entity\Shop
     */
    public function getStock()
    {
        return $this->stock;
    }



    /**
     * Set available
     *
     * @param boolean $available
     *
     * @return Stock
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }

    /**
     * Get available
     *
     * @return boolean
     */
    public function getAvailable()
    {
        return $this->available;
    }


    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Stock
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

}
