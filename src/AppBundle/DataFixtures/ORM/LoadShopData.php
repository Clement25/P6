<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Shop;
use AppBundle\Entity\Size;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadShopData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $shop = new Shop();

        $shop->setFirstName('Clément');
        $shop->setName('MICHELIN');
        $shop->setTitle('tee shirt de Clément');
        $shop->setContent('blablbalba');
        $shop->setBirthDate(new \DateTime('05-12-2016'));
        $shop->setImage($this->getReference('image1'));
        $shop->setDessinValid(true);


        $manager->persist($shop);


        $shop2 = new Shop();

        $shop2->setFirstName('Laura');
        $shop2->setName('Dupont');
        $shop2->setTitle('tee shirt de Laura');
        $shop2->setContent('balblalbal');
        $shop2->setBirthDate(new \DateTime('05-05-2017'));
        $shop2->setImage($this->getReference('image2'));
        $shop2->setDessinValid(true);


        $manager->persist($shop2);


        $shop3 = new Shop();

        $shop3->setFirstName('Paul');
        $shop3->setName('Labrousse');
        $shop3->setTitle('tee shirt de Paul');
        $shop3->setContent('balblalbal');
        $shop3->setBirthDate(new \DateTime('05-04-2017'));
        $shop3->setImage($this->getReference('image3'));
        $shop3->setDessinValid(true);


        $manager->persist($shop3);

        $shop4 = new Shop();

        $shop4->setFirstName('Paul');
        $shop4->setName('Dupuis');
        $shop4->setTitle('tee shirt de Paul');
        $shop4->setContent('balblalbal');
        $shop4->setBirthDate(new \DateTime('03-03-2017'));
        $shop4->setImage($this->getReference('image4'));
        $shop4->setDessinValid(false);

        $manager->persist($shop4);

        $shop5 = new Shop();

        $shop5->setFirstName('Jacques');
        $shop5->setName('Diesel');
        $shop5->setTitle('tee shirt de Jacques');
        $shop5->setContent('balblalbal');
        $shop5->setBirthDate(new \DateTime('05-05-2017'));
        $shop5->setImage($this->getReference('image5'));
        $shop5->setDessinValid(false);

        $manager->persist($shop5);


        $shop6 = new Shop();

        $shop6->setFirstName('Justine');
        $shop6->setName('Ferreux');
        $shop6->setTitle('tee shirt de Justine');
        $shop6->setContent('balblalbal');
        $shop6->setBirthDate(new \DateTime('14-08-2017'));
        $shop6->setImage($this->getReference('image6'));
        $shop6->setDessinValid(false);

        $manager->persist($shop6);

        $manager->flush();

        $this->addReference('dessin1', $shop);
        $this->addReference('dessin2', $shop2);
        $this->addReference('dessin3', $shop3);

        $this->addReference('stock1', $shop);
        $this->addReference('stock2', $shop2);
        $this->addReference('stock3', $shop3);
        $this->addReference('stock4', $shop4);
        $this->addReference('stock5', $shop5);
        $this->addReference('stock6', $shop6);

    }

    public function getOrder()
    {
        return 2;
    }

}