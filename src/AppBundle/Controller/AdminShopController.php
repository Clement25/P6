<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class AdminShopController extends Controller
{
    /**
     * Lists all shop entities.
     *
     * @Route("/admin", name="adminArticles_index")
     * @Method("GET")
     */
    public function indexAction()
    {

        $data = $this->get('app.back_shop')->index();

        return $this->render('back/shop/index.html.twig', array(
            'shops' => $data[0],
            'commandes' => $data[1],
        ));
    }

    /**
     * Creates a new shop entity.
     *
     * @Route("/admin/shop/new", name="adminArticles_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $data = $this->get('app.back_shop')->add($request);

        if ($request->isMethod('POST') && $data === null) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('adminArticles_index');
        }

        return $this->render('back/shop/new.html.twig', array(
            'shop' => $data[0],
            'form' => $data[1]->createView(),
        ));
    }

    /**
     * Finds and displays a shop entity.
     *
     * @Route("/admin/shop/{id}", name="adminArticles_show")
     * @Method("GET")
     */
    public function showAction(Shop $shop)
    {
        $deleteForm = $this->get('app.back_shop')->show($shop);

        return $this->render('back/shop/show.html.twig', array(
            'shop' => $shop,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing shop entity.
     *
     * @Route("/admin/shop/{id}/edit", name="adminArticles_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Shop $shop)
    {

        $data = $this->get('app.back_shop')->edit($request, $shop);

        if ($request->isMethod('POST') && $data === null) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adminArticles_edit', array('id' => $shop->getId()));
        }

        return $this->render('back/shop/edit.html.twig', array(
            'shop' => $shop,
            'delete_form' => $data[0]->createView(),
            'edit_form' => $data[1]->createView(),
        ));
    }


    /**
     * Valid a shop entity
     *
     * @Route("/admin/valid/{id}", name="adminArticles_valid")
     *
     */
    public function addValidationAction(Request $request)
    {

        $article = $this->get('app.back_shop')->addValidation($request);

        return $this->redirectToRoute('adminArticles_index', array(
            'article' => $article
        ));
    }


    /**
     * Valid a shop entity
     *
     * @Route("/admin/remove/{id}", name="adminArticles_remove")
     *
     */
    public function removeValidationAction(Request $request)
    {

        $article = $this->get('app.back_shop')->removeValidation($request);

        return $this->redirectToRoute('adminArticles_index', array(
            'article' => $article
        ));
    }


    /**
     * Deletes a shop entity.
     *
     * @Route("/admin/shop/{id}/delete", name="adminArticles_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Shop $shop)
    {

        $form = $this->get('app.back_shop')->delete($request, $shop);

        return $this->redirectToRoute('adminArticles_index', array(
            'form' => $form,
        ));
    }

}
