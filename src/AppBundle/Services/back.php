<?php

namespace AppBundle\Services;

use AppBundle\Entity\Article;
use AppBundle\Entity\Commande;
use AppBundle\Entity\Shop;
use AppBundle\Entity\Stock;
use AppBundle\Form\CommandeType;
use AppBundle\Form\ContactType;
use AppBundle\Form\ShopType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class Back
{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;


    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var AuthenticationUtils
     */
    private $authentificationUtils;

    private $sk;




    public function __construct(
        EntityManager $doctrine,
        Session $session,
        FormFactory $form,
        AuthenticationUtils $authenticationUtils,
        \Swift_Mailer $mailer,
        \Twig_Environment $twig,
        $sk
    )
    {
        $this->doctrine = $doctrine;
        $this->session = $session;
        $this->form = $form;
        $this->authentificationUtils = $authenticationUtils;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->sk = $sk;
    }


    public function shop()
    {

        $em = $this->doctrine;

        $listPhotosShop = $em->getRepository('AppBundle:Image')->findAll();

        $listShop = $em->getRepository('AppBundle:Shop')->findBy(
            array('dessinValid' => 1)
        );

        return [$listPhotosShop, $listShop];
    }


    public function shopDetail($id)
    {

        $em = $this->doctrine;

        $article = $em->getRepository('AppBundle:Stock')->findOneBy(array('stock' => $id));

        if (null === $article) {
            throw new Exception('l\'article d\'id ' . $id . ' n\'existe pas');
        }
        return [$article];
    }


    public function addArticle(Request $request, $id)
    {
        $session = $request->getSession();

        if (!$session->has('panier'))
            $session->set('panier', array());
        $panier = $session->get('panier');

        if (array_key_exists($id, $panier)) {
            if ($request->query->get('qte') !== null)
                $panier[$id] = $request->query->get('qte');
        } else {
            if ($request->query->get('qte') !== null)
                $panier[$id] = $request->query->get('qte');
            else
                $panier[$id] = 1;

        }
        $session->set('panier', $panier);

        return $this;
    }


    public function panier(Request $request)
    {
        $session = $request->getSession();

        if (!$session->has('panier')) $session->set('panier', array());

        $em = $this->doctrine;
        $produits = $em->getRepository('AppBundle:Stock')->findArray(array_keys($session->get('panier')));

        return [$produits, $session];

    }

    public function delivery(Request $request)
    {

        $session = $request->getSession();

        if (!$session->has('panier')) $session->set('panier', array());

        $produits = $this->doctrine->getRepository('AppBundle:Stock')->findArray(array_keys(($session->get('panier'))));

        if ($produits === null) {
            $response = new RedirectResponse('shop');
            $response->send();
        }

        $commande = new Commande();

        $form = $this->form->create(CommandeType::class, $commande);
        $session->get('commande');

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $data = $form->getData();
            $session->set('commande', $data);

           $response = new RedirectResponse('payment');
           $response->send();
        }

        return [$produits, $form, $session];

    }


    public function payment(Request $request)
    {
        $session = $request->getSession();

        if (!$session->has('panier')) $session->set('panier', array());

        $produits = $this->doctrine->getRepository('AppBundle:Stock')->findArray(array_keys($session->get('panier')));

        $commande = $session->get('commande');

        try {
            if ($commande === null) {
                $response = new RedirectResponse('delivery');
                $response->send();
            }

            if ($produits === null) {
                $response = new RedirectResponse('shop');
                $response->send();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $response = new RedirectResponse('shop');
            $response->send();
        }

        if ($request->isMethod('POST')) {
            $token = $request->get('stripeToken');

            \Stripe\Stripe::setApiKey($this->sk);

            \Stripe\Charge::create(array(
                "amount" => $commande->getTotal() * 100,
                "currency" => "EUR",
                "source" => $token,
                "description" => ""
            ));

            foreach ($produits as $produit) {

                $article = new Article();
                $article->setTitle($produit->getStock()->getTitle());
                $article->setContent($produit->getStock()->getContent());

                $article->setQuantity($session->get('panier')[$produit->getId()]);

                $article->setPrice($produit->getPrice());

                $commande->addArticle($article);

                $em = $this->doctrine;
                $em->persist($commande);
            }
            $em = $this->doctrine;
            $em->flush();

            $this->confirmationEmaiNewOrderlAction($request);
            $response = new RedirectResponse('confirmation');
            $response->send();
        }

        return [$commande, $produits];

    }


    public function confirmation(Request $request)
    {

        $session = $request->getSession();
        $commande = $session->get('commande');
        $produits = $session->get('panier');

        try {
            if ($commande === null) {
                $response = new RedirectResponse('delivery');
                $response->send();
            }

            if ($produits === null) {
                $response = new RedirectResponse('shop');
                $response->send();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $response = new RedirectResponse('shop');
            $response->send();
        }

        $this->session->getFlashBag()->add('info', 'La commande a bien été validée, vous allez recevoir
        un email de coessionfirmation dans quelques minutes.');

        return $session;
    }


    public function remove(Request $request, $id)
    {

        $session = $request->getSession();

        $panier = $session->get('panier');

        if (array_key_exists($id, $panier)) {
            unset($panier[$id]);
            $session->set('panier', $panier);
        }

        $this->session->getFlashBag()->add('info', 'L\'article a bien été supprimer!');
    }


    public function menu(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('panier'))
            $articles = 0;
        else $articles = count($session->get('panier'));

        return $articles;
    }


    public function newTeeshirt(Request $request)
    {

        $shop = new Shop();

        $em = $this->doctrine;

        $form = $this->form->create(ShopType::class, $shop);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $shop->setDessinValid(false);
            $em->persist($shop);
            $em->flush();
            $this->session->getFlashBag()->add('info', 'Votre dessin a bien été envoyer.');
        }

        return $form;


    }


    public function getConnexion()
    {
        $authentificationUtils = $this->authentificationUtils;
        $error = $authentificationUtils->getLastAuthenticationError();
        $lastUsername = $this->authentificationUtils->getLastUsername();

        return [$error, $lastUsername];
    }

    public function contactAction(Request $request)
    {

        $form = $this->form->create(ContactType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $message = \Swift_Message::newInstance()
                ->setSubject($data["objet"])
                ->setFrom($data['email'])
                ->setTo('saya25@live.fr')
                ->setBody(
                    $this->twig->render(
                    // app/Resources/views/email/email_contact.html.twig
                        'email/email_contact.html.twig', array(
                            'prenom' => $data['prenom'],
                            'nom' => $data['nom'],
                            'message' => $data['message'],
                            'email' => $data['email'],
                        )
                    ), 'text/html');

            $this->mailer->send($message);
        }

        return $form;
    }

    public function confirmationEmaiNewOrderlAction(Request $request)
    {

        $session = $request->getSession();
        $commande = $session->get('commande');

        $message = \Swift_Message::newInstance()
            ->setSubject("objet")
            ->setFrom($commande->getEmail())
            ->setTo('saya25@live.fr')
            ->setBody(
                $this->twig->render(
                // app/Resources/views/email/email_new_order.html.twig
                    'email/email_new_order.html.twig', array(
                        'referenceCommande' => $commande->getReferenceCommande(),
                        'articles' => $commande->getArticle(),
                        'total' => $commande->getTotal(),
                        'firstName' => $commande->getFirstName(),
                        'name' => $commande->getName(),
                        'email' => $commande->getEmail(),
                        'address' => $commande->getAddress(),
                        'postalCode' => $commande->getPostalCode(),
                        'city' => $commande->getCity(),
                    )
                ), 'text/html');

        $this->mailer->send($message);

    }
}

