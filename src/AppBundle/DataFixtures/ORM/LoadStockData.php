<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Stock;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadStockData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $stock = new Stock();

        $stock->setAvailable(true);
        $stock->setStock($this->getReference('stock1'));
        $stock->setPrice(10);


        $manager->persist($stock);

        $stock2 = new Stock();

        $stock2->setAvailable(true);
        $stock2->setStock($this->getReference('stock1'));
        $stock2->setPrice(10);


        $manager->persist($stock2);


        $stock3 = new Stock();

        $stock3->setAvailable(true);
        $stock3->setStock($this->getReference('stock1'));
        $stock3->setPrice(10);


        $manager->persist($stock3);


        $stock4 = new Stock();

        $stock4->setAvailable(true);
        $stock4->setStock($this->getReference('stock2'));
        $stock4->setPrice(9);


        $manager->persist($stock4);


        $stock5 = new Stock();

        $stock5->setAvailable(true);
        $stock5->setStock($this->getReference('stock2'));
        $stock5->setPrice(9);


        $manager->persist($stock5);


        $stock6 = new Stock();

        $stock6->setAvailable(true);
        $stock6->setStock($this->getReference('stock2'));
        $stock6->setPrice(9);


        $manager->persist($stock6);


        $stock7 = new Stock();

        $stock7->setAvailable(false);
        $stock7->setStock($this->getReference('stock2'));
        $stock7->setPrice(9);

        $manager->persist($stock7);

        $stock8 = new Stock();

        $stock8->setAvailable(true);
        $stock8->setStock($this->getReference('stock3'));
        $stock8->setPrice(9);

        $manager->persist($stock8);


        $manager->flush();

    }


    public function getOrder()
    {
        return 4;
    }

}