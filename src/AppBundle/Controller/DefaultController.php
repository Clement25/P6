<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Commande;
use AppBundle\Entity\Shop;
use AppBundle\Entity\Stock;
use AppBundle\Form\CommandeType;
use AppBundle\Form\ShopType;
use AppBundle\Form\StockType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/home", name="home")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render(':default:index.html.twig');
    }


    /**
     * @Route("/shop", name="shop")
     */
    public function shopAction()
    {

        $data = $this->get('app.back')->shop();

        // replace this example code with whatever you need
        return $this->render(':default:shop.html.twig', array(
            'listShop' => $data[1],
            'listPhotosShop' => $data[0],
        ));
    }


    /**
     * @Route("/shop/{id}/", name="detail_article")
     */
    public function shopDetailAction($id)
    {

        $data = $this->get('app.back')->shopDetail($id);

        $form = $this->createForm(StockType::class, $data[0]);

        // replace this example code with whatever you need
        return $this->render(':default:shop_details.html.twig', array(
            'article' => $data[0],
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/shop/add/{id}", name="add_article")
     *
     */
    public function addArticlelAction(Request $request, $id)
    {

        $data = $this->get('app.back')->addArticle($request, $id);

        return $this->redirectToRoute('panier', array('data' => $data));
    }

    /**
     * @Route("/panier", name="panier")
     * @Method({"GET"})
     */
    public function panierAction(Request $request)
    {

        $data = $this->get('app.back')->panier($request);

        return $this->render(':default:panier.html.twig', array(
            'produits' => $data[0],
            'panier' => $data[1]->get('panier'),
        ));
    }

    /**
     * @Route("panier/remove/{id}", name="remove")
     */
    public function removeAction(Request $request, $id)
    {

        $data = $this->get('app.back')->remove($request, $id);

        return $this->redirectToRoute('panier', array('data' =>$data));

    }


    /**
     * @Route("/delivery", name="delivery")
     */
    public function deliveryAction(Request $request)
    {

        $data = $this->get('app.back')->delivery($request);

        return $this->render(':default:delivery.html.twig', array(
            'produits' => $data[0],
            'form' => $data[1]->createView(),
            'panier' => $data[2]->get('panier')));

    }


    /**
     * @Route("/payment", name="payment")
     */
    public function paymentAction(Request $request)
    {

        $session = $request->getSession();

        $data = $this->get('app.back')->payment($request);

        return $this->render(':default:payment.html.twig', array(
            'commande' => $data[0],
            'panier' => $session->get('panier'),
            'produits' => $data[1],
            'public_key' => $this->getParameter("public_key"),
        ));
    }


    /**
     * @Route("/confirmation", name="confirmation")
     */
    public function confirmationAction(Request $request)
    {

        $data = $this->get('app.back')->confirmation($request);

        return $this->render(':default:confirmation.html.twig', array
        ('data' => $data ));
    }



    /**
     * @Route("/new_tee_shirt", name="new_tee_shirt")
     */
    public function newTeeShirtAction(Request $request)
    {

        $data = $this->get('app.back')->newTeeshirt($request);

            return $this->render(':default:new_tee_shirt.html.twig', array(
                'form' => $data->createView(),
            ));

    }


    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {

        $form = $this->get("app.back")->contactAction($request);

        return $this->render(':default:contact.html.twig', array(
            'form' => $form->createView(),
        ));

    }


    /**
     * @Route("/clear", name="clear")
     */
    public function clearAction(Request $request)
    {
        $clear = $request->getSession();
        $clear->clear();
        return $this->redirectToRoute('panier');
    }


    /**
     * @Route("/association", name="notre_association")
     */
    public function notreAssociationAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render(':default:association.html.twig');
    }


    /**
     * @Route("/mentionsLegales", name="mentions_legales")
     */
    public function mentionsLegalestAction(Request $request)
    {

        return $this->render(':default:mentions_legales.html.twig');

    }

    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexionAction()
    {
        $data = $this->get("app.back")->getConnexion();
        return $this->render(':default:connexion.html.twig', array(
            'last_username' => $data[1],
            'error' => $data[0],
        ));
    }

    public function menuAction(Request $request)
    {

        $articles = $this->get("app.back")->menu($request);

        return $this->render(':default:menu_panier.html.twig', array(
            'articles' => $articles));
    }



}
