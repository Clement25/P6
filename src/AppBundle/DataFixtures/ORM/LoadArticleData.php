<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Article;
use AppBundle\Entity\Commande;
use AppBundle\Entity\Shop;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadArticleData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $commande = new Commande();

        $commande->setReferenceCommande('ezajeoiz29ajuepoazkoei');
        $commande->setDateCommande(new \DateTime());
        $commande->setFirstName('Clément');
        $commande->setName('MICHELIN');
        $commande->setAddress('9 rue champ toine');
        $commande->setPostalCode(25300);
        $commande->setCity('Houtaud');
        $commande->setEmail('saya25@live.fr');
        $commande->setTotal(43);


        $article = new Article();


        $article->setTitle('tee shirt de Clément');
        $article->setContent('blablbalba');
        $article->setPrice(15);
        $article->setQuantity(2);


        $article2 = new Article();

        $article2->setTitle('tee shirt de Laura');
        $article2->setContent('balblalbal');
        $article2->setPrice(14);
        $article2->setQuantity(3);


        $article3 = new Article();

        $article3->setTitle('tee shirt de Paul');
        $article3->setContent('balblalbal');
        $article3->setPrice(14);
        $article3->setQuantity(3);


        $article->setCommande($commande);
        $article2->setCommande($commande);
        $article3->setCommande($commande);

        $manager->persist($article);
        $manager->persist($article2);
        $manager->persist($article3);

        $manager->flush();


        $this->addReference('article', $article);
        $this->addReference('article2', $article2);
        $this->addReference('article3', $article3);

    }


    public function getOrder()
    {
        return 4;
    }

}