<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommandeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array(
            'label' => 'Nom',
            'constraints' => array(
                new Length(array("min" => 2,
                    "max" => 50,
                    "minMessage" => "Il faut 2 caractères minimum",
                    "maxMessage" => "Il faut 50 caractères maximum"
                )),
                new NotBlank(array("message" => "Ce champ est obligatoire")),
            )))
            ->add('firstName', TextType::class, array(
                'label' => 'Prénom',
                'constraints' => array(
                    new Length(array("min" => 3,
                        "max" => 50,
                        "minMessage" => "Il faut 3 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'constraints' => array(
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('address', TextType::class, array(
                'label' => 'Adresse postale',
                'constraints' => array(
                    new Length(array("min" => 10,
                        "max" => 80,
                        "minMessage" => "Il faut 10 caractères minimum",
                        "maxMessage" => "Il faut 80 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('postalCode', IntegerType::class, array(
                'label' => 'Code postal',
                'constraints' => array(
                    new Length(array("min" => 5,
                        "max" => 10,
                        "minMessage" => "Il faut 5 caractères minimum",
                        "maxMessage" => "Il faut 10 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('city', TextType::class, array(
                'label' => 'Ville',
                'constraints' => array(
                    new Length(array("min" => 2,
                        "max" => 50,
                        "minMessage" => "Il faut 2 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('total', HiddenType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Commande'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_commande';
    }


}
