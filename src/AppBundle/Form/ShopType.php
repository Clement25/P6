<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ShopType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'label' => 'Prénom',
                'constraints' => array(
                    new Length(array("min" => 3,
                        "max" => 50,
                        "minMessage" => "Il faut 3 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('name', TextType::class, array(
                'label' => 'Nom',
                'constraints' => array(
                    new Length(array("min" => 2,
                        "max" => 50,
                        "minMessage" => "Il faut 2 caractères minimum",
                        "maxMessage" => "Il faut 50 caractères maximum"
                    )),
                    new NotBlank(array("message" => "Ce champ est obligatoire")),
                )))
            ->add('birthDate', DateType::class, array(
                'label' => 'Date de naissance'
            ))
            ->add('image', ImageType::class)
            ->add('envoyer', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Shop'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_shop';
    }


}
